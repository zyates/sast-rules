// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/ajinabraham/njsscan/blob/master/tests/assets/node_source/true_positives/semantic_grep/traversal/express_hbs_lfr.js
// hash: e7a0a61
module.exports = function (app) {
    // http://localhost:3000/traversal/express-lfr/1?template=index 
    app.get("/traversal/express-lfr/1", async (req, res) => {
        const template = req.query.template
        // ruleid: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(template, { title: "Title" })
    });

    // http://localhost:3000/traversal/express-lfr/2?template=user 
    app.get("/traversal/express-lfr/2", async (req, res) => {
        const template = req.query.template.concat('/user')
        // ruleid: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(template, { title: "User Page" })
    });

    // http://localhost:3000/traversal/express-lfr/3?template=user 
    app.get("/traversal/express-lfr/3", async (req, res) => {
        const template = req.query.template + '/user'
        // ruleid: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(template, { title: "User Page" })
    });

    // http://localhost:3000/traversal/express-lfr/4?template=user
    app.get("/traversal/express-lfr/4", async (req, res) => {
        var indexPath = req.query.template;
        indexPath = indexPath  + "/user"
        // ruleid: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(indexPath, { title: "User Page" })
    });

    // http://localhost:3000/traversal/express-lfr/5?template=user
    app.get("/traversal/express-lfr/5", async (req, res) => {
        var indexPath = req.query.template;
        indexPath = indexPath.concat("/user");
        // ruleid: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(indexPath, { title: "User Page" })
    });
    
    // http://localhost:3000/traversal/express-lfr/6?template=../views2/user
    app.get("/traversal/express-lfr/6", async (req, res) => {
        const path = require('path')
        // ruleid: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(req.query.template, { title: "User Page" })
    });

    // http://localhost:3000/traversal/express-lfr/safe/1
    app.get("/traversal/express-lfr/safe/1", async (req, res) => {
        // ok: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render("user/user", { title: "User Page" })
    });
    
    // http://localhost:3000/traversal/express-lfr/safe/2
    app.get("/traversal/express-lfr/safe/2", async (req, res) => {
        var indexPath = "index";
        // ok: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(indexPath, { title: "Index Page" })
    });

    // http://localhost:3000/traversal/express-lfr/safe/3
    app.get("/traversal/express-lfr/safe/3", async (req, res) => {
        var indexPath = "user/";
        indexPath  += "user"
        // ok: rules_lgpl_javascript_traversal_rule-express-lfr
        res.render(indexPath, { title: "User Page" })
    });
}