# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_ftp_rule-AvoidTaintedFTPCall"
  mode: taint
  pattern-sources:
  - pattern: params
  - pattern: cookies
  - pattern: request.env
  pattern-sinks:
  - pattern-either:
    - pattern: Net::FTP.$X(...)
    - patterns:
      - pattern-inside: |
          $FTP = Net::FTP.$OPEN(...)
          ...
          $FTP.$METHOD(...)
      - pattern: $FTP.$METHOD(...)
  message: |
    The application was found calling the Net::FTP modules methods with
    user supplied input. A malicious actor could use this to modify or access files
    they should not have access to.
    
    Directly incorporating user-controlled input from `params`, `cookies`, 
    or `request.env` into FTP commands or connection setups can lead to 
    various security vulnerabilities, including Remote Code Execution 
    (RCE), unauthorized file access, and data exfiltration. It's crucial 
    to validate, sanitize, and, where possible, avoid using user-controlled 
    data in sensitive operations like FTP transactions. Ensure that any input 
    used in FTP operations is strictly controlled and validated against 
    expected patterns.

    Remediation Strategy:
    To mitigate these risks, ensure that all user-provided input is strictly 
    validated and sanitized before being used in FTP operations. 
    - Verify the format and content of the input to ensure it meets expected 
    criteria.
    - Use allowlists to restrict the input to known safe values.
    - Employ built-in security features of the programming language or 
    framework to escape or safely handle user input.

    Secure Code Example:
    ```
    filename = params[:filename]
    # Validate the filename to ensure it's a known, safe file
    raise "Invalid filename" unless filename =~ /\A[\w]+\.\w+\z/

    Net::FTP.open('example.com', 'user', 'password') do |ftp|
      # Proceed with the FTP operation using the validated and sanitized filename
      ftp.getbinaryfile(filename, "local_#{filename}", 1024)
    end
    ```
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    owasp:
    - "A1:2017-Injection"
    - "A03:2021-Injection"
    cwe: "CWE-76"
    shortDescription: "Improper neutralization of equivalent special elements"    
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/audit/avoid-tainted-ftp-call.yaml"
    category: "security"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    - "rails"