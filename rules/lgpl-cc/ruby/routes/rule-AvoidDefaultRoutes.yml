# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_routes_rule-AvoidDefaultRoutes"
  patterns:
  - pattern-either:
    - pattern: map.connect ":controller/:action/:id"
    - pattern: match ':controller(/:action(/:id(.:format)))'
  paths:
    include:
    - '*routes.rb'
  message: |
    Default routes are enabled in this routes file. This means any public method on a
    controller can be called as an action. It is easy to accidentally expose
    methods. Instead, remove the offending line and explicitly include all
    routes that are intended for external users to follow.

    Secure Code Example:
    ```
    # In routes.rb
    Rails.application.routes.draw do
      get 'posts' => 'posts#index', as: :posts
      get 'posts/:id' => 'posts#show', as: :post
      post 'posts' => 'posts#create'

      # Other CRUD actions for posts can be defined similarly
    end
    ```
  languages: 
  - "ruby"
  severity: "WARNING"
  metadata:
    owasp:
    - "A5:2017-Broken Access Control"
    - "A01:2021-Broken Access Control"
    cwe: "CWE-276"
    shortDescription: "Incorrect Default Permissions"
    security-severity: "MEDIUM"
    references:
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/rails/security/audit/xss/avoid-default-routes.yaml"
    category: "security"
    technology:
    - "rails"
  