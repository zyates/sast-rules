# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: "java_xpathi_rule-XpathInjection"
    languages:
      - "java"
    message: |
      The application processes `XPath` queries with potentially malicious input.
      An adversary who is able to control the XPath query could potentially influence the logic
      of how data is retrieved, processed or even bypass protections.

      To mitigate the vulnerability, avoid using user input in XPath queries.
      If it cannot be avoided, validate the user input against an allowlist
      of expected values before using it in the query. 

      Following is an example that demonstrates how to perform an XPath query 
      using user input validated against an allowlist.

      Secure code example:
      ```
      public class SecureXPathWithoutResolver {

        private static final List<String> ALLOWED_AUTHORS = List.of("Author1", "Author2", "Author3");

        public static void main(String[] args) {
          try {
            String userInput = "Author1"; // Example user input
              if (ALLOWED_AUTHORS.contains(userInput)) {
                // User input is safe to use
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document doc = builder.parse("path/to/your/xmlfile.xml");

                XPathFactory xpathFactory = XPathFactory.newInstance();
                XPath xpath = xpathFactory.newXPath();

                // Safely embed the validated user input into the XPath expression
                String expression = String.format("//books/book[author='%s']", userInput);
                NodeList nodes = (NodeList) xpath.evaluate(expression, doc, XPathConstants.NODESET);

                for (int i = 0; i < nodes.getLength(); i++) {
                    System.out.println(nodes.item(i).getTextContent());
                }
              } else {
                throw new IllegalArgumentException("Input is not allowed.");
              }
          } catch (Exception e) {
              e.printStackTrace();
          }
        }
      }
      ```

      For more information on XPath Injection see:
      - https://owasp.org/www-community/attacks/XPATH_Injection
    metadata:
      shortDescription:
        "Improper neutralization of data within XPath expressions ('XPath
        Injection')"
      category: "security"
      cwe: "CWE-643"
      owasp:
        - "A1:2017-Injection"
        - "A03:2021-Injection"
      security-severity: "CRITICAL"
    mode: "taint"
    pattern-sources:
      - pattern-either:
          - pattern: |
              (HttpServletRequest $REQ).$FUNC(...)
          - patterns:
              - pattern-inside: |
                  $FUNC(..., $VAR, ...) {
                  ...
                  }
              - pattern: "$VAR"
    pattern-sinks:
      - pattern-either:
          - patterns:
              - pattern-inside: |
                  (javax.xml.xpath.XPath $XP). ... .evaluate($EXP, ...);
              - pattern: "$EXP"
          - patterns:
              - pattern-inside: |
                  (javax.xml.xpath.XPath $XP). ... .compile($EXP, ...);
              - pattern: "$EXP"
    pattern-sanitizers:
      - patterns:
          - pattern-inside: $MAP.getOrDefault($VAR,"...");
          - pattern: $VAR
      - patterns:
          - pattern-either:
              - pattern: |
                  if($VALIDATION){
                  ...
                  }
              - patterns:
                  - pattern-inside: |
                      $A = $VALIDATION;
                      ...
                  - pattern: |
                      if($A){
                        ...
                      }
          - metavariable-pattern:
              metavariable: $VALIDATION
              pattern-either:
                - pattern: |
                    $AL.contains(...)
                - pattern: |
                    $AL.equals(...)
    severity: "ERROR"
