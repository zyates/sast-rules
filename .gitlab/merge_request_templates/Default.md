## What does this MR do?

<!--
Describe in detail what your merge request does, why it does that, etc.

Please also keep this description up-to-date with any discussion that takes
place so that reviewers can understand your intent. This is especially
important if they didn't participate in the discussion.

Make sure to remove this comment when you are done.
-->

## What are the relevant issue numbers?
<!-- Add links for all issue numbers that are relevant for this merge request. -->

## Does this MR meet the acceptance criteria?

- [ ] The test cases cover both positive and negative cases and are also annotated with appropriate semgrep annotations:
  - For positive cases: `// ruleid: ...`
  - For negative cases: `// ok: ....`
- [ ] Following metadata fields exist for the rule(s) added/updated in this MR:
	- `owasp` with both 2017 and 2021 mappings.
	- `category: "security"`
	- `cwe`
	- `shortDescription`
    - `security-severity`
- [ ] The message field is valid and contains a secure code example.
- [ ] Applicable license is mentioned in the rule if embedded/taken from external source.
- [ ] Relevant labels including workflow labels are appropriately selected.


<!-- Change "type::feature" to "type::maintenance" if you're removing rules or to "type::bug" if you're fixing a bug -->
/label ~"type::feature" ~"devops::secure" ~"section::sec" ~"group::static analysis" ~"Category:SAST" ~"SAST::Ruleset" ~"backend"


<!-- Choose the priority for SAST::Ruleset -->
<!-- /label ~"SAST::Ruleset::P1" -->
<!-- /label ~"SAST::Ruleset::P2" -->
<!-- /label ~"SAST::Ruleset::P3" -->
<!-- /label ~"SAST::Ruleset::P4" -->


<!-- Choose one of the feature subtypes if using type::feature -->
<!-- /label ~"feature::addition" -->
<!-- /label ~"feature::removal" -->
<!-- /label ~"feature::enhancement" -->
<!-- /label ~"feature::consolidation" -->